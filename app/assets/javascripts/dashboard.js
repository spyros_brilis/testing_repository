// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require jquery-ui/effect
//= require jquery.cookies
//= require bootstrap
//= require underscore
//= require socket.io
//= require select2
//= require sweet-alert.min
//= require dashboard/modernizr.min
//= require dashboard/pace.min
//= require dashboard/wow.min
//= require dashboard/jquery.scrollTo.min.js
//= require dashboard/jquery.nicescroll.js
//= require dashboard/jquery.app
//= require handlebars.runtime
//= require ansi_up.js
//= require react
//= require react_ujs
//= require react_bootstrap
//= require components
//
//
//= require init
//= require_tree ./dashboard
//= require autoload
