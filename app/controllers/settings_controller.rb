class SettingsController < DashboardController
  include Controllers::EnsureProject

  def show
  end

  def worker_setup
  end

  def notifications
  end
end
