# See http://www.robotstxt.org/robotstxt.html for documentation on how to use the robots.txt file
#
# To ban all spiders from the entire site uncomment the next two lines:
User-agent: *
Allow: /

Sitemap: http://s3-us-west-2.amazonaws.com/testributor/sitemaps/sitemap.xml.gz
